/** create and connect to bmarkletDB */
function dbConnection() {
  window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
  window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction;
  window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;

  if (!window.indexedDB) {
    alert("Your browser does not support this feature.");
  }

  //create db
  window.request = window.indexedDB.open("bmarkletDB");

  request.onerror = (event) => {
    console.error("Error while opening indexeddb.", event);
    return reject(event);
  };

  request.onupgradeneeded = (event) => {
    window.db = event.target.result;

    //table that holds all records
    window.followResult = window.db.createObjectStore("followResults", { autoIncrement: true });

    //table that holds all scans
    window.followResult = window.db.createObjectStore("scans", { autoIncrement: true });
  };

  request.onsuccess = (event) => {
    window.db = event.target.result;
  };
}

/** adds a user object to the followResults table */
function dbAddObject(user, link, result_date) {
  const tx = window.db.transaction("followResults", "readwrite");
  const pFollowResults = tx.objectStore("followResults");

  pFollowResults.add({ user, link, result_date, page_username: window.username });

  return new Promise((resolve) => {
    tx.oncomplete = () => {
      return resolve(0);
    };
  });
}

/** adds a scan object to the scans table */
function dbAddScan(date) {
  const tx = window.db.transaction("scans", "readwrite");
  const pScans = tx.objectStore("scans");

  pScans.add({ date: date, page_username: window.username });

  return new Promise((resolve) => {
    tx.oncomplete = () => {
      return resolve(0);
    };
  });
}

/** loops through all results and adds record to indexeddb */
async function dbAddResults(users) {
  const result_date = new Date().getTime();

  await dbAddScan(result_date);

  for (user in users) {
    await dbAddObject(users[user].user, users[user].link, result_date);
  }

  console.log(users);
}

function dbGetAllScans() {
  const tx = window.db.transaction("scans");

  const pScans = tx.objectStore("scans");

  const request = pScans.openCursor();

  const result = [];

  return new Promise((resolve) => {
    request.onsuccess = (e) => {
      const cursor = e.target.result;

      if (cursor) {
        if (cursor.value.page_username == window.username) {
          result.push(cursor.value.date);
        }

        cursor.continue();
      } else {
        return resolve(result);
      }
    };
  });
}

/** converts time from unix timestamp  */
function timeConverter(UNIX_timestamp) {
  var a = new Date(UNIX_timestamp);
  var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = a.getHours();
  var min = a.getMinutes();
  var sec = a.getSeconds();
  var time = date + " " + month + " " + year + " " + hour + ":" + min + ":" + sec;
  return time;
}

/** gets folowers scanned at a specific date */
function dbGetDateFollowers(result_date) {
  const tx = window.db.transaction("followResults");

  const pScans = tx.objectStore("followResults");

  const request = pScans.openCursor();

  const result = [];

  return new Promise((resolve) => {
    request.onsuccess = (e) => {
      const cursor = e.target.result;

      if (cursor) {
        if (cursor.value.result_date == result_date && cursor.value.page_username == window.username) {
          result.push(cursor.value);
        }

        cursor.continue();
      } else {
        console.log(result);
        return resolve(result);
      }
    };
  });
}

/** sorts new follows and unfollows */
function sortNewFollows(new_list, old_list) {
  const new_list_usernames = [];
  const old_list_usernames = [];

  const new_follows = [];
  const new_unfollows = [];

  for (n in new_list) {
    new_list_usernames.push(new_list[n].user);
  }

  for (n in old_list) {
    old_list_usernames.push(old_list[n].user);
  }

  //get new follows
  for (n in new_list_usernames) {
    let username = new_list_usernames[n];

    if (old_list_usernames.includes(username) === false) {
      new_follows.push(username);
    }
  }

  //get new unfollows
  for (n in old_list_usernames) {
    let username = old_list_usernames[n];

    if (new_list_usernames.includes(username) === false) {
      new_unfollows.push(username);
    }
  }

  if (new_follows.length < 1 && new_unfollows.length < 1) {
    return alert("No difference");
  }

  if (new_follows.length > 0) {
    alert(`You started following: ${new_follows.join(" - ")} .`);
  }

  if (new_unfollows.length > 0) {
    alert(`You unfollowed: ${new_unfollows.join(" - ")} .`);
  }

  return { new_follows, new_unfollows };
}

/**
 * compares last scan with a specific scan
 * @param {number} index
 * @param {Array} all_scans
 */
async function compareWithLast(index, all_scans, last_scan_date) {
  if (!index || !all_scans || !last_scan_date) throw "cannot compare";

  const selected_date = all_scans[index];

  if (!selected_date) {
    alert("Selected scan must be in range");
    return;
  }

  //get last scan followers
  const last_scan_followers = await dbGetDateFollowers(last_scan_date);

  //get selected scan followers
  const selected_date_followers = await dbGetDateFollowers(selected_date);

  console.log({ selected_date_followers, last_scan_followers });

  sortNewFollows(last_scan_followers, selected_date_followers);
}

/**
 * start
 * TODO: check that we're on a "follows" page
 */

function start() {
  dbConnection();

  window.userCellCollection = [];

  window.scrollTo(0, 0);

  let username = window.location.href;

  if (!username.includes("twitter.com")) {
    return alert("This bookmarklet works with twitter");
  }

  username = "@" + username.replace("https://twitter.com/", "").replace("https://www.twitter.com/", "").split("/")[0];

  window.username = username;

  var command = prompt(`Please enter a number to run a command:\n[1] Crawl Follows for ${username}\n[2] Compare`);

  if (!command) return alert("This cannot be empty");

  if (command == "1") return getMoreFollows();

  if (command == "2") return compare();

  return alert("UNKNOWN COMMAND");
}
start();

/**
 * getMoreFollows
 * scroll down the follows page and collects UserCell DOM elements into window.userCellCollection
 * it should continue to gather UserCell elements until it doesn't find any more.
 */
function getMoreFollows() {
  var newRecs = 0;
  var userCells = document.querySelectorAll("[aria-label='Timeline: Following'] [data-testid='UserCell']");

  userCells.forEach((currentDiv) => {
    //console.log(currentDiv)
    if (window.userCellCollection.indexOf(currentDiv) == -1 && currentDiv != undefined) {
      //console.log("adding div")
      window.userCellCollection.push(currentDiv);
      newRecs++;
    }
  });
  var lastOne = userCells[userCells.length-1];
  // console.log(lastOne);
  lastOne.scrollIntoView();
  //window.scroll(0, window.innerHeight + window.scrollY);

  if (newRecs > 0) {
    console.log(`Logged ${newRecs} new followed accounts.`);
    setTimeout(getMoreFollows, 1000);
  } else {
    var cont = confirm(
      `You follow ${window.userCellCollection.length} accounts.  Log these results to your local database?`
    );
    if (cont == true) {
      getUserObjs();
    }
  }
}

/**
 * getUserObjs
 * extracts usable account data from list of UserCells DOM elements.
 */
function getUserObjs() {
  window.userObjCollection = [];

  window.userCellCollection.forEach((cell) => {
    let thisUserDiv = cell.firstChild.children[1].firstChild.firstChild.firstChild;
    let userObj = {
      user: thisUserDiv.children[1].querySelector("span").innerHTML,
      //            "name": thisUserDiv.querySelector('span').firstChild.innerHTML,
      link: thisUserDiv.querySelector("a").href,
    };
    window.userObjCollection.push(userObj);
  });

  dbAddResults(window.userObjCollection);
}

/**
 * TODO:
 * compare
 * Compares how the followers list has changes between selected scans.
 */
async function compare() {
  //all scans
  const previous_scans = await dbGetAllScans();

  const last_scan_date = previous_scans.slice(-1);

  //remove last scan (beacause its used for comparison)
  previous_scans.pop();

  let previous_scans_text = "";

  for (n in previous_scans) {
    previous_scans_text = previous_scans_text + `[${n}] ` + timeConverter(previous_scans[n]) + "\n";
  }

  //if scans are less than one (last scan not included)
  if (previous_scans.length < 1) {
    alert("You have no scans to compare with");
    return;
  }

  var command = prompt(
    `You can compare the last scan with ${previous_scans.length} previous one(s), which one would you like to compare with:\n\n${previous_scans_text}`
  );

  if (command != null) {
    compareWithLast(command, previous_scans, last_scan_date);
  }
}
