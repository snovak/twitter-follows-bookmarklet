# twitter-follows-bookmarklet

https://snovak.gitlab.io/twitter-follows-bookmarklet/out.html


## Usage

<video src="assets/usage.mp4" width="800px" controls></video>  

### Getting Started

    npm install
    npm run build

...will build twitter-follows.js into `out.html`, which you should copy into a bookmarklet in your bookmarks toolbar.

